"""Web application to extract text and words from wep page."""
# app.py
#
import sys
import os

try:
    from flask import Flask
except ImportError:
    dir_path = os.path.dirname(os.path.realpath(__file__))
    sys.path.insert(0, os.path.join(dir_path, "dependencies"))

import settings

from views import Homepage


app = Flask(__name__, static_url_path='/static')

# router
app.add_url_rule('/', view_func=Homepage.as_view('homepage'))

if __name__ == '__main__':
    app.run(
        settings.BIND_ADDRESS,
        settings.BIND_PORT,
        debug=settings.SERVER_DEBUG)
