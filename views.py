# views.py
import requests

import urllib3

from flask.views import MethodView
from flask import render_template, request

import settings
from parser import Parser


urllib3.disable_warnings()


def get_letters(text):
    return dict(
        (c, text.count(c)) for c in text if (
            c >= 'a' and c <= 'z' or ord(c) >= 128))


class Homepage(MethodView):
    methods = ['GET', 'POST']
    template_name = "test.html"

    def get_template_name(self):
        return self.template_name

    def render_template(self, context):
        return render_template(self.get_template_name(), **context)

    def get_url_response(self, url):
        try:
            response = requests.get(
                url=url, allow_redirects=True,
                timeout=settings.REQUEST_TIMEOUT,
                verify=False
            )
        except:
            raise
        return response

    def get_objects(self, data={}, response=None):
        p = Parser()
        p.parse(response.content)

        words_freqs = {}
        for w in set(p.get_words()):
            words_freqs[w] = p.get_words().count(w)
        data['words'] = sorted(
            words_freqs.items(), key=lambda x: x[1], reverse=True)

        data['longest'] = p.get_longest_word()

        whole = u""
        for content in p.get_tags_content():
            whole += u" " + content

        letters = get_letters(whole)

        most_common = []
        current = 1
        for l in sorted(letters.items(), key=lambda x: x[1], reverse=True):
            if l[1] >= current:
                most_common.append(l[0])
                current = l[1]
        data['letters'] = most_common

        return data

    def get_context(self, url=None):
        data = {}
        if not url:
            return data

        data['url'] = url
        try:
            response = self.get_url_response(url)
        except Exception as e:
            data['error'] = e
            return data
        if response.status_code != requests.codes.ok:
            data['warn'] = "Response code:{0}".format(response.status_code)
            return data

        data['info'] = "Response code:{0}".format(response.status_code)

        data = self.get_objects(data, response)
        return data

    def get(self):
        return self.render_template(self.get_context())

    def post(self):
        url = request.form['url']
        return self.render_template(self.get_context(url))
