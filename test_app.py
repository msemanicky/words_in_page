# -*- encoding:utf-8 -*-
import unittest
from parser import Parser

from views import get_letters


class ParseHtmlTest(unittest.TestCase):
    HTML = u"<html><head><hu>testčťžhu</hu><title>testtitle</title>\
<title>Testtitle  </title><title>Testtitle</title></head><body>\
testbody<dev>this is test test dev</dev></body>\
oh noooo       oo oo \t  oo  \n\n </html>"
    PARSED = [u"testtitle", u"testbody", u"testčťžhu", u"testtitle  ",
              u"this is test test dev",
              u"oh noooo       oo oo \t  oo  \n\n ",
              ]
    WORDS = [u"testtitle", u"testbody", u"testčťžhu", u"this", u"is", u"test",
             u"dev", "oh", "noooo", "oo",
             ]

    LONGEST = [u"testčťžhu", u"testtitle", ]

    def setUp(self):
        self.p = Parser()
        self.p.parse(self.HTML)

    def test_parse_html_to_texts(self):
        self.assertSetEqual(set(self.p.get_tags_content()), set(self.PARSED))

    def test_get_words(self):
        self.assertSetEqual(set(self.p.get_words()), set(self.WORDS))

    def test_get_longest_words(self):
        self.assertSetEqual(set(self.p.get_longest_word()), set(self.LONGEST))


class ViewFunctionsTest(unittest.TestCase):
    def test_letters(self):
        text = u"this is test message"
        result = {u'a': 1, u'e': 3, u'g': 1, u'i': 2, u'h': 1, u'm': 1,
                  u's': 5, u't': 3}
        self.assertDictEqual(get_letters(text), result)


if __name__ == '__main__':
    unittest.main()
