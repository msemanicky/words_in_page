"""Module to parse HTML content"""
from lxml import html
import re


class Parser(object):

    tags_items = []
    words = []

    def __get_text(self, obj):
        if obj is not None:
            if obj.getchildren():
                for item in obj.getchildren():
                    self.__get_text(item)
            try:
                if obj.text:
                    self.tags_items.append(obj.text.lower())
                    self.words.extend(
                        re.compile('\w+', re.UNICODE).findall(obj.text.lower()))
            except:
                pass

    def parse(self, text=u""):
        """Parse HTML."""
        """Extract inner text between tags, and words."""

        self.tags_items = []
        self.words = []
        parsed = html.fromstring(text)
        for item in parsed:
            self.__get_text(item)

    def get_tags_content(self):
        return self.tags_items

    def get_words(self):
        return self.words

    def get_longest_word(self):
        words = sorted(list(set(self.get_words())), key=len, reverse=True)
        index = 0
        first_word = words[index]
        for word in words:
            if len(word) < len(first_word):
                break
            index += 1

        return words[0:index]

    def __init__(self):
        self.tags_items = []
        self.words = []
